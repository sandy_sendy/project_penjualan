-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2017 at 09:30 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_penjualan`
--

-- --------------------------------------------------------

--
-- Table structure for table `trx_makanan`
--

CREATE TABLE IF NOT EXISTS `trx_makanan` (
  `id_trx` int(10) NOT NULL,
  `no_pesanan` varchar(255) NOT NULL,
  `id_makanan` int(10) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_minuman`
--

CREATE TABLE IF NOT EXISTS `trx_minuman` (
  `id_trx` int(10) NOT NULL,
  `no_pesanan` varchar(255) NOT NULL,
  `id_minuman` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_pesanan`
--

CREATE TABLE IF NOT EXISTS `trx_pesanan` (
  `no_pesanan` varchar(255) NOT NULL,
  `no_meja` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `waktu_input` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_makanan`
--

CREATE TABLE IF NOT EXISTS `t_makanan` (
  `id_makanan` int(10) NOT NULL,
  `nm_makanan` varchar(255) NOT NULL,
  `ket` varchar(255) NOT NULL,
  `status_makanan` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_makanan`
--

INSERT INTO `t_makanan` (`id_makanan`, `nm_makanan`, `ket`, `status_makanan`) VALUES
(2, 'Mie Goreng', 'mie goreng jawa', 'Ready'),
(3, 'nasi goreng', 'nasi goreng jawa', 'Ready'),
(4, 'Mie Ayam', 'mie ayam jawa', 'Ready'),
(6, 'Ketoprak', 'ketoprak ', 'No Ready'),
(7, 'tes2222', 'tes', 'No Ready');

-- --------------------------------------------------------

--
-- Table structure for table `t_minuman`
--

CREATE TABLE IF NOT EXISTS `t_minuman` (
  `id_minuman` int(10) NOT NULL,
  `nm_minuman` varchar(255) NOT NULL,
  `ket` varchar(255) NOT NULL,
  `status_minuman` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_minuman`
--

INSERT INTO `t_minuman` (`id_minuman`, `nm_minuman`, `ket`, `status_minuman`) VALUES
(1, 'Es teh', 'es teh manis', 'Ready'),
(2, 'Jus Jeruk', 'Es Jus Jeruk Asli', 'Ready'),
(3, 'Jus Mangga', 'jus mangga', 'No Ready');

-- --------------------------------------------------------

--
-- Table structure for table `t_pesanan`
--

CREATE TABLE IF NOT EXISTS `t_pesanan` (
  `id_pesanan` int(10) NOT NULL,
  `no_pesanan` varchar(255) NOT NULL,
  `nm_makanan` varchar(255) NOT NULL,
  `nm_minuman` varchar(255) NOT NULL,
  `no_meja` varchar(255) NOT NULL,
  `status_pesanan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `no_tlp` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id`, `username`, `password`, `no_tlp`, `alamat`, `status`) VALUES
(1, 'admin', 'c3284d0f94606de1fd2af172aba15bf3', '0989898', 'kebagusan', 'Admin'),
(2, 'sandy', 'd686a53fb86a6c31fa6faa1d9333267e', '0988988', 'keparakan', 'Kasir');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `trx_makanan`
--
ALTER TABLE `trx_makanan`
  ADD PRIMARY KEY (`id_trx`);

--
-- Indexes for table `trx_minuman`
--
ALTER TABLE `trx_minuman`
  ADD PRIMARY KEY (`id_trx`);

--
-- Indexes for table `trx_pesanan`
--
ALTER TABLE `trx_pesanan`
  ADD PRIMARY KEY (`no_pesanan`);

--
-- Indexes for table `t_makanan`
--
ALTER TABLE `t_makanan`
  ADD PRIMARY KEY (`id_makanan`);

--
-- Indexes for table `t_minuman`
--
ALTER TABLE `t_minuman`
  ADD PRIMARY KEY (`id_minuman`);

--
-- Indexes for table `t_pesanan`
--
ALTER TABLE `t_pesanan`
  ADD PRIMARY KEY (`id_pesanan`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `trx_makanan`
--
ALTER TABLE `trx_makanan`
  MODIFY `id_trx` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trx_minuman`
--
ALTER TABLE `trx_minuman`
  MODIFY `id_trx` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_makanan`
--
ALTER TABLE `t_makanan`
  MODIFY `id_makanan` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `t_minuman`
--
ALTER TABLE `t_minuman`
  MODIFY `id_minuman` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_pesanan`
--
ALTER TABLE `t_pesanan`
  MODIFY `id_pesanan` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
