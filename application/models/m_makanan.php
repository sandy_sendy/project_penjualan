<?php
class M_makanan extends CI_model{
   		
		function __construct(){
			parent::__construct();
			
		}
		
	public function create()
	{
		return $this->db->insert('t_makanan',array(
			'nm_makanan'		=>$this->input->post('nm_makanan',true),
			'ket'				=>$this->input->post('ket',true),
			'status_makanan'	=>$this->input->post('status_makanan',true),
			
		));
	}
	
	public function update($id)
	{
		$this->db->where('id_makanan', $id);
		return $this->db->update('t_makanan',array(
			'nm_makanan'	=>$this->input->post('nm_makanan',true),
			'ket'			=>$this->input->post('ket',true),
			'status_makanan'=>$this->input->post('status_makanan',true)
		));
	}
	
	public function delete($id)
	{
		return $this->db->delete('t_makanan', array('id_makanan' => $id)); 
	}
	
	public function getJson()
	{
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id_makanan';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'asc';
		$offset = ($page-1) * $rows;
		
		$result = array();
		$result['total'] = $this->db->get('t_makanan')->num_rows();
		$row = array();
		
		$this->db->limit($rows,$offset);
		$this->db->order_by($sort,$order);
		$criteria = $this->db->get('t_makanan');
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'id'=>$data['id_makanan'],
				'nm_makanan'	=>$data['nm_makanan'],
				'ket'			=>$data['ket'],
				'status_makanan'=>$data['status_makanan']
			);
		}
		$result=array_merge($result,array('rows'=>$row));
		return json_encode($result);
	}
}