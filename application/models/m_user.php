<?php
class M_user extends CI_model{
   		
		function __construct(){
			parent::__construct();
			
		}
		
	public function create()
	{
		return $this->db->insert('t_user',array(
			'username'		 =>$this->input->post('username',true),
			'password'	=>md5($this->input->post("password")),
			'no_tlp'   	=>$this->input->post('no_tlp',true),
			'alamat'   	=>$this->input->post('alamat',true),
			'status'   	=>$this->input->post('status',true)
			
		));
	}
	
	public function update($id)
	{
		$this->db->where('id', $id);
		return $this->db->update('t_user',array(
			'username'	=>$this->input->post('username',true),
			'password'	=>md5($this->input->post("password")),
			'no_tlp'   	=>$this->input->post('no_tlp',true),
			'alamat'   	=>$this->input->post('alamat',true),
			'status'   	=>$this->input->post('status',true)
		));
	}
	
	public function delete($id)
	{
		return $this->db->delete('t_user', array('id' => $id)); 
	}
	
	public function getJson()
	{
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'asc';
		$offset = ($page-1) * $rows;
		
		$result = array();
		$result['total'] = $this->db->get('t_user')->num_rows();
		$row = array();
		
		$this->db->limit($rows,$offset);
		$this->db->order_by($sort,$order);
		$criteria = $this->db->get('t_user');
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'id'			=>$data['id'],
				'username'		=>$data['username'],
				'password'		=>$data['password'],
				'no_tlp'		=>$data['no_tlp'],
				'alamat'		=>$data['alamat'],
				'status'		=>$data['status']
			);
		}
		$result=array_merge($result,array('rows'=>$row));
		return json_encode($result);
	}
}