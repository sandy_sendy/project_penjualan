<?php
class M_minuman extends CI_model{
   		
		function __construct(){
			parent::__construct();
			
		}
		
	public function create()
	{
		return $this->db->insert('t_minuman',array(
			'nm_minuman'		=>$this->input->post('nm_minuman',true),
			'ket'				=>$this->input->post('ket',true),
			'status_minuman'	=>$this->input->post('status_minuman',true),
			
		));
	}
	
	public function update($id)
	{
		$this->db->where('id_minuman', $id);
		return $this->db->update('t_minuman',array(
			'nm_minuman'	=>$this->input->post('nm_minuman',true),
			'ket'			=>$this->input->post('ket',true),
			'status_minuman'=>$this->input->post('status_minuman',true)
		));
	}
	
	public function delete($id)
	{
		return $this->db->delete('t_minuman', array('id_minuman' => $id)); 
	}
	
	public function getJson()
	{
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id_minuman';
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'asc';
		$offset = ($page-1) * $rows;
		
		$result = array();
		$result['total'] = $this->db->get('t_minuman')->num_rows();
		$row = array();
		
		$this->db->limit($rows,$offset);
		$this->db->order_by($sort,$order);
		$criteria = $this->db->get('t_minuman');
		
		foreach($criteria->result_array() as $data)
		{	
			$row[] = array(
				'id'=>$data['id_minuman'],
				'nm_minuman'	=>$data['nm_minuman'],
				'ket'			=>$data['ket'],
				'status_minuman'=>$data['status_minuman']
			);
		}
		$result=array_merge($result,array('rows'=>$row));
		return json_encode($result);
	}
}