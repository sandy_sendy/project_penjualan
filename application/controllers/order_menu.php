<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_menu extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model(array('m_order_menu'));
			
		}	
	public function index()
	{			
		$data['title'] 		= 'Input Order Menu';
		$data['list_makanan']  = $this->m_order_menu->get_makanan();
		$data['list_minuman']  = $this->m_order_menu->get_minuman();
		$data['main'] 		= 'order_menu/order_menu_v';			
		$this->load->view('template', $data);			
	}	
		
	function save_order(){
		
		$no_pesanan = $this->m_order_menu->cek_no_pesanan();
		$data = array(
					   array(
							   'no_pesanan'   => $this->input->post('no_pesanan'),							   
							   'no_meja'      => $this->input->post('no_pesanan'),							   
							   'username'  	  => $this->input->post('username'),							   
							   'waktu_input'  => date('H:i:s d-m-Y')							   
							   )
					);
		$this->m_order_menu->insert($data);		
		$this->session->set_flashdata('message', 'Data Berhasil Disimpan!');
		redirect('order_menu', 'refresh');
		
		
	}		
}

?>