<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Makanan extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model(array('m_makanan'));
			
		}	
	
	public function index()
	{		
		 if(isset($_GET['grid'])){
			 echo $this->m_makanan->getJson();
		 }
		 else{
			$data['title'] = 'Setting Makanan';
			$data['main'] = 'makanan/makanan_v';			
			$this->load->view('template', $data);	
		 }
	}
	
	public function create()
	{
		if(!isset($_POST))	
			show_404();
		
		if($this->m_makanan->create())
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Gagal memasukkan data'));
	}
	
	public function update($id=null)
	{
		if(!isset($_POST))	
			show_404();
		
		if($this->m_makanan->update($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Gagal mengubah data'));
	}
	
	public function delete()
	{
		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['id']));
		if($this->m_makanan->delete($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Gagal menghapus data'));
	}
}
