<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
        parent::__construct();
        
        //$this->load->model(array('m_login'));
    }
    
    function index(){
		
        $data['title'] = 'Aplikasi Penjualan';
		//$data['get_all'] = $this->m_acara->get_all();
		$data['main'] = 'home/home_v';			
		$this->load->view('template', $data);
    }
}
?>