<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
        parent::__construct();
        
        $this->load->model(array('m_login'));
    }
    
    function index(){
		
        $this->load->view('login/login_v');
    }
	
	function aksi_login(){
		$this->load->model(array('m_login'));
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password)
			);
		$cek = $this->m_login->cek_login("t_user",$where)->num_rows();
		if($cek > 0){
 
			$data_session = array(
				'nama' => $username,
				'status' => "login"
				);
 
			$this->session->set_userdata($data_session);
 
			redirect('home');
 
		}else{
			$this->session->set_flashdata('message', 'Maaf, username dan atau password Anda salah');
			redirect('login');
		}
	}
 
		
		
		// Logout from admin page
	public function logout() {	
		$this->session->sess_destroy();
		redirect('login', 'refresh');
		}

}
