<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class minuman extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model(array('m_minuman'));
			
		}	
	
	public function index()
	{		
		 if(isset($_GET['grid'])){
			 echo $this->m_minuman->getJson();
		 }
		 else{
			$data['title'] = 'Setting minuman';
			$data['main'] = 'minuman/minuman_v';			
			$this->load->view('template', $data);	
		 }
	}
	
	public function create()
	{
		if(!isset($_POST))	
			show_404();
		
		if($this->m_minuman->create())
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Gagal memasukkan data'));
	}
	
	public function update($id=null)
	{
		if(!isset($_POST))	
			show_404();
		
		if($this->m_minuman->update($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Gagal mengubah data'));
	}
	
	public function delete()
	{
		if(!isset($_POST))	
			show_404();
		
		$id = intval(addslashes($_POST['id']));
		if($this->m_minuman->delete($id))
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('msg'=>'Gagal menghapus data'));
	}
}
