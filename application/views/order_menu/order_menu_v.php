<h1><i class="fa fa-shopping-cart   fa-fw"></i> Order Menu</h1>
<form role="form">
	 <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-cutlery fa-fw"></i>  Makanan 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Makanan</th>
                                            <th>Status</th>
                                            <th>Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php 
											$i = 1;
											foreach($list_makanan as $row) {
												$id_makanan = $row->id_makanan ;
												
												?>
                                        <tr>										
                                            <td><?php echo $i++.'.'; ?></td>
                                            <td><?php echo $row->nm_makanan; ?></td>
                                            <td><?php echo $row->status_makanan; ?></td>
											<?php if($row->status_makanan =='Ready') { 
												echo "<td><input type='text' name='id_makanan_$id_makanan' size='3'></td>";
											} else {
												echo "<td><input type='text'  id='disabledInput' disabled=''  size='3'></td>";
											} ?>
                                            
                                        </tr>
											<?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-cutlery fa-fw"></i>  Makanan 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Makanan</th>
                                            <th>Status</th>
                                            <th>Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php 
											$i = 1;
											foreach($list_minuman as $row) {?>
                                        <tr>										
                                            <td><?php echo $i++.'.'; ?></td>
                                            <td><?php echo $row->nm_minuman; ?></td>
                                            <td><?php echo $row->status_minuman; ?></td>
											<?php if($row->status_minuman =='Ready') { 
												echo "<td><input type='text' name='id_minuman' size='3'></td>";
											} else {
												echo "<td><input type='text'  id='disabledInput' disabled=''  size='3'></td>";
											} ?>
                                            
                                        </tr>
											<?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
               
     </div>
           
		   
	</br>
	<button type="button" class="btn btn-primary"><i class="fa fa-hand-o-up  fa-fw"></i> Pesan</button>
</form>
                                   