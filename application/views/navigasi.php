<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<li class="sidebar-search">
				<div class="input-group custom-search-form">
					<input type="text" class="form-control" placeholder="Search...">
					<span class="input-group-btn">
					<button class="btn btn-default" type="button">
						<i class="fa fa-search"></i>
					</button>
				</span>
				</div>
				<!-- /input-group -->
			</li>
			<li>
				<a href="<?php echo base_url().'home' ; ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
			</li>                      
			<li>
				<a href="<?php echo base_url().'user' ; ?>"><i class="fa fa-group fa-fw"></i> User</a>
			</li>
			<li>
				<a href="<?php echo base_url().'makanan' ; ?>"><i class="fa fa-cutlery fa-fw"></i> Makanan</a>
			</li>
			<li>
				<a href="<?php echo base_url().'minuman' ; ?>"><i class="fa fa-glass fa-fw"></i> Minuman</a>
			</li>
			
			<li>
				<a href="<?php echo base_url().'order_menu' ; ?>"><i class="fa fa-shopping-cart   fa-fw"></i> Order Menu</a>
			</li>
		    <li>
				<a href="<?php echo base_url().'status_pesanan' ; ?>"><i class="fa fa-bookmark fa-fw"></i> Status Pesanan</a>
			</li>
		    <li>
				<a href="<?php echo base_url().'stok_pesanan' ; ?>"><i class="fa fa-asterisk fa-fw"></i> Stok Pesanan</a>
			</li>
		    <li>
				<a href="<?php echo base_url().'Laporan' ; ?>"><i class="fa fa-windows fa-fw"></i> Laporan</a>
			</li>
		 
		   
			
		</ul>
	</div>
	<!-- /.sidebar-collapse -->
</div>