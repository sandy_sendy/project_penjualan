<script type="text/javascript">
var url;

function create(){
	jQuery('#dialog-form').dialog('open').dialog('setTitle','Tambah User');
	jQuery('#form').form('clear');
	url = '<?php echo site_url('user/create'); ?>';
}

function update(){
	var row = jQuery('#datagrid-crud').datagrid('getSelected');
	if(row){
		jQuery('#dialog-form').dialog('open').dialog('setTitle','Edit User');
		jQuery('#form').form('load',row);
		url = '<?php echo site_url('user/update'); ?>/' + row.id;
	}
}

function save(){
	jQuery('#form').form('submit',{
		url: url,
		onSubmit: function(){
			return jQuery(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if(result.success){
				jQuery('#dialog-form').dialog('close');
				jQuery('#datagrid-crud').datagrid('reload');
			} else {
				jQuery.messager.show({
					title: 'Error',
					msg: result.msg
				});
			}
		}
	});
}

function remove(){
	var row = jQuery('#datagrid-crud').datagrid('getSelected');
	if (row){
		jQuery.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){
			if (r){
				jQuery.post('<?php echo site_url('user/delete'); ?>',{id:row.id},function(result){
					if (result.success){
						jQuery('#datagrid-crud').datagrid('reload');
					} else {
						jQuery.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				},'json');
			}
		});
	}
}
</script>

<!-- Data Grid -->
<table id="datagrid-crud" title="Setting user" class="easyui-datagrid" style="width:auto; height: auto;" url="<?php echo site_url('user/index');?>?grid=true" toolbar="#toolbar" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" collapsible="true">
	<thead>
		<tr>
			<th field="id" width="30" sortable="true">ID</th>
			<th field="username" width="50" sortable="true">Username</th>
			
			<th field="no_tlp" width="50" sortable="true">No Tlp</th>
			<th field="alamat" width="50" sortable="true">alamat</th>
			<th field="status" width="50" sortable="true">Status User</th>
			
		</tr>
	</thead>
</table>

<!-- Toolbar -->
<div id="toolbar">
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="create()">Tambah user</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="update()">Edit user</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="remove()">Hapus user</a>
</div>

<!-- Dialog Form -->
<div id="dialog-form" class="easyui-dialog" style="width:400px; height:350px; padding: 10px 20px" closed="true" buttons="#dialog-buttons">
	<form id="form" method="post" novalidate>
		<div class="form-item">
			<label for="type">Username</label><br />
			<input type="text" name="username" class="easyui-validatebox" required="true" size="53" maxlength="50" />
		</div>
		<div class="form-item">
			<label for="type">Password</label><br />
			<input type="text" name="password" class="easyui-validatebox" required="true" size="53" maxlength="50" />
		</div>
		<div class="form-item">
			<label for="type">Alamat</label><br />
			<input type="text" name="alamat" class="easyui-validatebox" required="true" size="53"  />
		</div>
		<div class="form-item">
			<label for="type">No. Tlp</label><br />
			<input type="text" name="no_tlp" class="easyui-validatebox" required="true" size="53" maxlength="50" />
		</div>
		
		
		<div class="form-item">
			<label for="type">Status</label><br />
			<select name="status" class="required form-control">						
					
					<option value='Admin'> Admin </option>
					<option value='Kasir'> Kasir </option>
					<option value='Pelayan'> Pelayan </option>
					
				</select>
				</div>
		
	</form>
</div>

<!-- Dialog Button -->
<div id="dialog-buttons">
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Simpan</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:jQuery('#dialog-form').dialog('close')">Batal</a>
</div>